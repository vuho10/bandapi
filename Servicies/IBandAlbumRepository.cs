﻿using BandAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BandAPI.Servicies
{
        public interface IBandAlbumRepository
        {
            IEnumerable<Album> GetAlbums(Guid bandId);
            Album GetAlbum(Guid bandId, Guid albumId);
            void AddAlbum(Guid bandId, Album album);
            void UpdateAblum(Album album);
            void DeleteAblum(Album album);

        IEnumerable<Band> GetBands();
        Band GetBand(Guid bandId);
        IEnumerable<Band> GetBands(IEnumerable<Guid> bandId);
        void AddBand(Band band);
        void UpdateBand(Band band);
        void DeleteBand(Band band);

        bool BandExists(Guid bandId);
        bool AlbumExists(Guid albumId);
        bool save();
        }
}
