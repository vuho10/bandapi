﻿using BandAPI.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BandAPI.DBContexts
{
    public class BandAlbumContext : DbContext
    {
        internal object Albums;

        public BandAlbumContext(DbContextOptions<BandAlbumContext> options) : base(options)
        {

        }

        public DbSet<Band> bands { get; set; }
        public DbSet<Album> albums { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Band>().HasData(new Band()
            {
                Id = Guid.Parse("023f5275-0069-4f6b-91a1-d461b5ada2a4"),
                Name = "One",
                Founded = new DateTime(1980, 1, 1),
                MainGenre = "Rock"
            },
            new Band
            {
                Id = Guid.Parse("dda08520-8a42-4670-912e-46f2b3a0bbde"),
                Name = "Two",
                Founded = new DateTime(1990, 2, 2),
                MainGenre = "POP"
            },
            new Band
            { 
                Id = Guid.Parse("4375382d-4ead-4f09-8ffe-4e0e6cd18e2d"),
                Name = "Three",
                Founded = new DateTime(2000, 3, 3),
                MainGenre = "Romantic"
            });

            modelBuilder.Entity<Album>().HasData(new Album()
            {
                Id = Guid.Parse("cae79ab6-f957-459a-9577-5e7407e7506b"),
                Title = "KingDom One",
                Description = "One of the One",
                BandId = Guid.Parse("023f5275-0069-4f6b-91a1-d461b5ada2a4")
            },
            new Album
            {
                Id = Guid.Parse("874deb7b-3202-465f-83fe-d0f160a15e80"),
                Title = "KingDom Two",
                Description = "One of the Two",
                BandId = Guid.Parse("dda08520-8a42-4670-912e-46f2b3a0bbde")
            },
            new Album
            {
                Id = Guid.Parse("3ff21fc7-aa81-4288-81de-d6567d9b2c33"),
                Title = "KingDom Three",
                Description = "One of the Three",
                BandId = Guid.Parse("4375382d-4ead-4f09-8ffe-4e0e6cd18e2d")
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}
