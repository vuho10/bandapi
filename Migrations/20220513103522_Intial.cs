﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BandAPI.Migrations
{
    public partial class Intial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "bands",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Founded = table.Column<DateTime>(nullable: false),
                    MainGenre = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bands", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "albums",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 200, nullable: false),
                    Description = table.Column<string>(maxLength: 200, nullable: true),
                    Band = table.Column<string>(nullable: true),
                    BandId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_albums", x => x.Id);
                    table.ForeignKey(
                        name: "FK_albums_bands_BandId",
                        column: x => x.BandId,
                        principalTable: "bands",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "bands",
                columns: new[] { "Id", "Founded", "MainGenre", "Name" },
                values: new object[] { new Guid("023f5275-0069-4f6b-91a1-d461b5ada2a4"), new DateTime(1980, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Rock", "One" });

            migrationBuilder.InsertData(
                table: "bands",
                columns: new[] { "Id", "Founded", "MainGenre", "Name" },
                values: new object[] { new Guid("dda08520-8a42-4670-912e-46f2b3a0bbde"), new DateTime(1990, 2, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "POP", "Two" });

            migrationBuilder.InsertData(
                table: "bands",
                columns: new[] { "Id", "Founded", "MainGenre", "Name" },
                values: new object[] { new Guid("4375382d-4ead-4f09-8ffe-4e0e6cd18e2d"), new DateTime(2000, 3, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "Romantic", "Three" });

            migrationBuilder.InsertData(
                table: "albums",
                columns: new[] { "Id", "Band", "BandId", "Description", "Title" },
                values: new object[] { new Guid("cae79ab6-f957-459a-9577-5e7407e7506b"), null, new Guid("023f5275-0069-4f6b-91a1-d461b5ada2a4"), "One of the One", "KingDom One" });

            migrationBuilder.InsertData(
                table: "albums",
                columns: new[] { "Id", "Band", "BandId", "Description", "Title" },
                values: new object[] { new Guid("874deb7b-3202-465f-83fe-d0f160a15e80"), null, new Guid("dda08520-8a42-4670-912e-46f2b3a0bbde"), "One of the Two", "KingDom Two" });

            migrationBuilder.InsertData(
                table: "albums",
                columns: new[] { "Id", "Band", "BandId", "Description", "Title" },
                values: new object[] { new Guid("3ff21fc7-aa81-4288-81de-d6567d9b2c33"), null, new Guid("4375382d-4ead-4f09-8ffe-4e0e6cd18e2d"), "One of the Three", "KingDom Three" });

            migrationBuilder.CreateIndex(
                name: "IX_albums_BandId",
                table: "albums",
                column: "BandId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "albums");

            migrationBuilder.DropTable(
                name: "bands");
        }
    }
}
